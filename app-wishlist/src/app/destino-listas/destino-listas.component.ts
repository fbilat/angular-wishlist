import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-destino-listas',
  templateUrl: './destino-listas.component.html',
  styleUrls: ['./destino-listas.component.css']
})
export class DestinoListasComponent implements OnInit {

  // creamos un array vacio
  destinos: DestinoViaje[];

  constructor() {
    // el array creado lo iniciamos con datos
  this.destinos = [];
  }

  ngOnInit(): void {
  }

  // funcion para agregar un valor nuevo a la lista
  guardar(nombre:string,url:string):boolean {
    // agregamos datos (push)
    this.destinos.push(new DestinoViaje(nombre,url));
    return false;
  }

}
