import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinoListasComponent } from './destino-listas.component';

describe('DestinoListasComponent', () => {
  let component: DestinoListasComponent;
  let fixture: ComponentFixture<DestinoListasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DestinoListasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinoListasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
