import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { DestinoListasComponent } from './destino-listas/destino-listas.component';

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    DestinoListasComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
